package com.example.x_o_gamee

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    lateinit var firstPlayerName: EditText
    lateinit var secondPlayerName: EditText
    lateinit var startButton: Button
    lateinit var maxscore : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firstPlayerName = findViewById(R.id.FirstPlayer)
        secondPlayerName = findViewById(R.id.SecondPlayer)
        startButton = findViewById(R.id.StartButton)

        startButton.setOnClickListener {

            val firstPlayer : String = firstPlayerName.text.toString().trim()
            val secondPlayer : String = secondPlayerName.text.toString().trim()
            if (firstPlayer.isEmpty()){

                firstPlayerName.setText("Player1")

            }
            if (secondPlayer.isEmpty()){

                secondPlayerName.setText("Player2")

            }

            val firstPlayer1 : String = firstPlayerName.text.toString().trim()
            val secondPlayer2 : String = secondPlayerName.text.toString().trim()

            val intent = Intent(this, GamePage:: class.java)

            intent.putExtra("First", firstPlayer1)
            intent.putExtra("Second", secondPlayer2)
            startActivity(intent)



        }


    }
}